const disp1 = document.getElementById("display1");
const disp2 = document.getElementById("display2");

let PointIndicator = 0;
let result = "";

function getInput(val) {
    const input1 = disp1.getAttribute("value");
    const input2 = disp2.getAttribute("value");    
    const lastDigit = input1[input1.length - 1];
    switch (val) {
        case "C":
            disp1.setAttribute("value", "");
            disp2.setAttribute("value", "");
            result = "";
            PointIndicator = 0;
            break;
        case "⌫":
            if (input1.length === 0) {
                break;
            } else if (lastDigit === "."){
                disp1.setAttribute("value", input1.slice(0,input1.length - 2));
                result = result.slice(0, result.length - 2);
                PointIndicator = 0;
                break; 
            } if (lastDigit === "÷" || lastDigit === "x" || lastDigit === "-" || lastDigit === "+") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2));
                result = result.slice(0,result.length - 2)
                result = input2;
                break; 
            } else {
                disp1.setAttribute("value", input2.slice(0, input2.length - 2));
                disp2.setAttribute("value", input1.slice(0, input2.length - 2));
                result = result.slice(0, result.length - 2);
            }
        case "=":
            if(input1.length === 0 || lastDigit === "÷" || lastDigit === "x" || lastDigit === "-" || lastDigit === "+" || lastDigit === ".") {
                break;
            } else {
                disp1.setAttribute("value", input2);
                result = input2;
                PointIndicator = 0;
                break;
            }
        case ".":
            if (PointIndicator === 1 || input1.length === 0) {
                break;
            } else if (lastDigit === "÷" || lastDigit === "x" || lastDigit === "-" || lastDigit === "+") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2) + ".");
                result = result.slice(0,result.length - 2) + "."
                PointIndicator = 1;
                break;
            } else {
                disp1.setAttribute("value", input1 + ".");
                result = result + ".";
                PointIndicator = 1;
                break;
            }
        case "÷":
            if (input1.length === 0) {
                break;
            } else if (lastDigit === "." || lastDigit === "x" || lastDigit === "-" || lastDigit === "+") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2) + "÷");
                result = result.slice(0,result.length - 2) + "/";
                break;
            } else {
                disp1.setAttribute("value", input1 + "÷");
                result = result + "/";
                break;
            }
        case "x":
            if (input1.length ===0) {
                break;
            } else if (lastDigit === "." || lastDigit === "÷" || lastDigit === "-" || lastDigit === "+") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2) + "x");
                result = result.slice(0, result.length - 2) + "*";
                break;
            } else {
                disp1.setAttribute("value", input1 + "x");
                result = result + "*";
                break;
            }
        case "-":
            if (lastDigit === "." || lastDigit === "+") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2) + "-");
                result = result.slice(0, result.length - 2) + "-";
                break;
            } else {
                disp1.setAttribute("value", input1 + "-");
                result = result + "-";
                break;
            }
        case "+":
            if (lastDigit === "." || lastDigit === "-") {
                disp1.setAttribute("value", input1.slice(0,input1.length - 2) + "+");
                result = result.slice(0, result.length - 2) + "+";
                break;
            } else {
                disp1.setAttribute("value", input1 + "+");
                result = result + "+";
                break;
            }
        default:
            let temp = "";
            disp1.setAttribute("value", input1 + val);
            result = result + val;
            temp = eval(result);
            disp2.setAttribute("value", temp.slice(0,18));
            break;
    }
}